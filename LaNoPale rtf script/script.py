import json
import codecs

test = 'liste_renvois.rtf'
out_file = open(test,'w')
out_file.write("""{\\rtf1\\mac\\ansicpg1252\\deff2 {\\fonttbl{\\f0\\fswiss Chicago;}{\\f2\\froman New York;}{\\f3\\fswiss Geneva;}{\\f4\\fmodern Monaco;}{\\f11\\fnil Cairo;}{\\f13\\fnil Zapf Dingbats;}{\\f14\\fnil Bookman;}{\\f15\\fnil N Helvetica Narrow;}{\\f16\\fnil Palatino;}{\\f18\\fnil Zapf Chancery;}
{\\f20\\froman Times;}{\\f21\\fswiss Helvetica;}{\\f22\\fmodern Courier;}{\\f23\\ftech Symbol;}{\\f33\\fnil Avant Garde;}{\\f34\\fnil New Century Schlbk;}{\\f1297\\fnil GoudyHundred;}{\\f1602\\fnil BlackChancery;}{\\f2515\\fnil MT Extra;}{\\f4950\\fnil TTYFont;}
{\\f11132\\fnil InsigniaLQmono;}{\\f11133\\fnil InsigniaLQprop;}{\\f32500\\fnil VT320;}{\\f32525\\fnil VT100;}}{\\colortbl\\red0\\green0\\blue0;\\red0\\green0\\blue255;\\red0\\green255\\blue255;\\red0\\green255\\blue0;\\red255\\green0\\blue255;\\red255\\green0\\blue0;
\\red255\\green255\\blue0;\\red255\\green255\\blue255;}{
\\stylesheet{\\s250\\li720 \\f21\\fs20\\ul \\sbasedon0\\snext0 heading 6;}
{\\s251\\li720 \\b\\f21\\fs20 \\sbasedon0\\snext0 heading 5;}
{\\s252\\li360 \\f21\\ul \\sbasedon0\\snext0 heading 4;}
{\\s253\\li360 \\b\\f21 \\sbasedon0\\snext0 heading 3;}
{\\s254\\sb120 \\b\\f21 \\sbasedon0\\snext0 heading 2;}
{\\s255\\sb240 \\b\\f21\\ul \\sbasedon0\\snext0 heading 1;}
{\\f21 \\sbasedon222\\snext0 Normal;}
{\\s2 \\b\\f21\\cf1 \\sbasedon0\\snext2 Anchor;}}
{\\info{\\author Vincent ETIEN}}""")

out_file.write("""\\margl720\\margr720\\ftnbj\\fracwidth \\sectd \\sbknone\\linemod0\\linex0\\cols1\\endnhere \\pard\\plain \\s255\\sb240 \\b\\f21\\ul Liste des renvois \\par""")

with codecs.open('renvois.json', 'r', "utf8") as json_file:
    data = json.load(json_file)
    current_arret=''
    for p in data:
        if current_arret!=p['src']:
            current_arret=p['src']
            out_file.write("""\\par\\pard\\plain \\s252\\li360 \\f21\\ul Arrêt """+p['src']+"""\\par """)
        out_file.write("""\\par\\pard\\plain \\s251\\li720 \\b\\f21\\fs20 §"""+str(p['npara'])+""" , renvoi vers """+p['dst']+"""\\b0\\par """)
        if p['obj2']!="":
            out_file.write("""\\plain \\fs20 \\ul Renvoi(s) secondaire(s) vers :\\ul0  """+p['obj2'])
            if p['obj3']!="":
                if p['obj4'] != "":
                    out_file.write(""", """)
                else :
                    out_file.write(""" ET """)
                out_file.write(p['obj3'])
                if p['obj4']!="":
                    if p['obj5'] != "":
                        out_file.write(""", """)
                    else:
                        out_file.write(""" ET """)
                    out_file.write(p['obj4'])
                    if p['obj5'] != "":
                        if p['obj6'] != "":
                            out_file.write(""", """)
                        else:
                            out_file.write(""" ET """)
                        out_file.write(p['obj5'])
                        if p['obj6'] != "":
                            if p['obj7'] != "":
                                out_file.write(""", """)
                            else:
                                out_file.write(""" ET """)
                            out_file.write(p['obj6'])
                            if p['obj7'] != "":
                                out_file.write(""" ET """ + p['obj7'])
            out_file.write("""\\par""")
        out_file.write("""\\pard\\plain \\fs20      \\ul Objet (Forme) :\\ul0  """+p['objet_forme']+""",   \\ul Type de citation :\\ul0  """+p['type']+"""\\par""")
        out_file.write("""      \\ul Objet (Fond) :\\ul0  """+p['objet_fond']+""",      \\ul Fonction (Fond) :\\ul0  """+p['fonction_fond']+"""\\par""")
        out_file.write("""      \\ul Extrait :\\ul0  \"""" +p['extrait']+"""\"\\par""")
        out_file.write("""      \\ul Notes :\\ul0  """ + p['remarque'] + """\\par""")




out_file.write("""}""")
out_file.close()