
async function repartitionTypes() {

    document.getElementById("contentPopup2").innerHTML="";
    let renvois =await d3.json("data/renvois.json");
    let types = renvois.map(r => {
        return r.type
    });
    let names = [], data = [], prev;
    types.sort();
    for ( let i = 0; i < types.length; i++ ) {
        if ( types[i] !== prev ) {
            names.push(types[i]);
            data.push(1);
        } else {
            data[data.length-1]++;
        }
        prev = types[i];
    }
    data = names.map( (t,i)=>{
        let obj = new Object();
        obj.label=t;
        obj.value=data[i];
        return obj
    });
    let pie = new d3pie("contentPopup2", {
        header: {
            title: {
                text: "Répartition des "+types.length+" renvois selon leur type"
            }
        },
        data: {
            content: data
        },
        "labels": {
            "inner": {
                "format": "value",
                "hideWhenLessThanPercentage": 3
            },
            "mainLabel": {
                "fontSize": 16
            },
            "value": {
                "color": "#000",
                "fontSize": 23
            }
        },

//Here further operations/animations can be added like click event, cut out the clicked pie section.
        callbacks: {
            onMouseoverSegment: function(info) {
            },
            onMouseoutSegment: function(info) {
            }
        },
        size: {
            "canvasHeight": 600,
            "canvasWidth": 900,
            "pieInnerRadius": "60%",
            "pieOuterRadius": "75%"
        },

    });
}

async function nbRenvois(){
    document.getElementById("contentPopup2").innerHTML="<br><text id=\"p1_title\" class=\"p1_title\" x=\"352.5\" y=\"25.015625\" text-anchor=\"middle\" fill=\"#333333\" style=\"font-size: 18px; font-family: arial;\">Nombre de renvois aux documents en fonction de leur date</text><br><br>";
    let data2 = await  d3.json("data/arrets.json");
    let data3 = await  d3.json("data/renvois.json");

    let data4 = [];
    let newrenv;
    data3.forEach(renv => {
        data4.push(renv)
        if (renv.obj2 !== ""&&renv.obj2!==undefined) {
            newrenv = jQuery.extend({}, renv);
            newrenv.dst = renv.obj2;
            data4.push(newrenv)
        }
        if (renv.obj3 !== ""&&renv.obj3!==undefined) {
            newrenv = jQuery.extend({}, renv);
            newrenv.dst = renv.obj3;
            data4.push(newrenv)
        }
        if (renv.obj4 !== ""&&renv.obj4!==undefined) {
            newrenv = jQuery.extend({}, renv);
            newrenv.dst = renv.obj4;
            data4.push(newrenv)
        }
        if (renv.obj5 !== ""&&renv.obj5!==undefined) {
            newrenv = jQuery.extend({}, renv);
            newrenv.dst = renv.obj5;
            data4.push(newrenv)
        }
        if (renv.obj6 !== ""&&renv.obj6!==undefined) {
            newrenv = jQuery.extend({}, renv);
            newrenv.dst = renv.obj6;
            data4.push(newrenv)
        }
        if (renv.obj7 !== ""&&renv.obj7!==undefined) {
            newrenv = jQuery.extend({}, renv);
            newrenv.dst = renv.obj7;
            data4.push(newrenv)
        }
    })
    data3 = data4

    //enlève les renvois qui sont fait entre des noeuds non-présents dans la base de donnée (à cause du tri)
    let names = data2.map(a => a.nom);
    data3 = data3.filter(r=>{
        return (names.includes(r.src)&&names.includes(r.dst))
    });


    //création des objets links et nodes nécéssaires à d3JS avec les valeurs des noeuds pour leur taille etc..
    const links =  data3.map(function(d){
        let sourceVal =-1;
        let targetVal=-2;
        let i=0;
        data2.forEach(function(ar){
            if(ar.nom===d.src){
                sourceVal=i;
            }
            if(ar.nom===d.dst){
                targetVal=i;
            }
            i++;
        });
        return Object.create(d, { displayed: {value:0,writable: true} ,source: {value: sourceVal,writable: true} ,target: {value:targetVal,writable: true} })
    });
    let nodes =  data2.map((d, index) => Object.create(d, { id: { value: index } }));
    nodes = nodes.filter(d=>d.annee!==0).map(d=> {
        let nb=0;
        links.forEach(function(renv){
            if(renv.target===d.id){
                nb++
            }
        });
        d.value=nb;
        return d
    } );

    nodes =nodes .sort((a, b) => (a.annee > b.annee) ? 1 : (a.annee === b.annee) ? ((a.mois > b.mois) ? 1: (a.mois === b.mois) ? ((a.jour > b.jour) ? 1 : -1) : -1) : -1 );

    // set the dimensions and margins of the graph
    var margin = {top: 10, right: 30, bottom: 30, left: 60},
        width = 460 - margin.left - margin.right,
        height = 400 - margin.top - margin.bottom;


// append the svg object to the body of the page
    var svg = d3.select("#contentPopup2")
        .append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
        .append("g")
        .attr("transform",
            "translate(" + margin.left + "," + margin.top + ")");

//Read the data


    // Add X axis --> it is a date format
    var x = d3.scaleTime()
        .domain(d3.extent(nodes, function(d) { return new Date(d.annee,d.mois-1,d.jour );}))
        .range([ 0, width ]);
    svg.append("g")
        .attr("transform", "translate(0," + height + ")")
        .call(d3.axisBottom(x));

    // Add Y axis
    var y = d3.scaleLinear()
        .domain( [0, Math.max(...nodes.map(d=>{return d.value}))])
        .range([ height, 0 ]);
    svg.append("g")
        .call(d3.axisLeft(y));

    // Add the line
    svg.append("path")
        .datum(nodes)
        .attr("fill", "none")
        .attr("stroke", "black")
        .attr("stroke-width", 1.5)
        .attr("d", d3.line()
            .curve(d3.curveCatmullRom) // Just add that to have a curve instead of segments
            .x(function(d) { return x(new Date(d.annee,d.mois-1,d.jour )) })
            .y(function(d) { return y(d.value) })
        );

    // create a tooltip
    var Tooltip = d3.select("#contentPopup2")
        .append("div")
        .style("opacity", 0)
        .attr("class", "tooltip")
        .style("background-color", "white")
        .style("border", "solid")
        .style("border-width", "2px")
        .style("border-radius", "5px")
        .style("padding", "5px");

    // Three function that change the tooltip when user hover / move / leave a cell
    var mouseover = function(d) {
        Tooltip
            .style("opacity", 1)
    };
    var mousemove = function(d) {
        Tooltip
            .html("Arrêt " + d.nom)
            .style("left", (d3.mouse(this)[0]+70) + "px")
            .style("top", (d3.mouse(this)[1]) + "px")
    };
    var mouseleave = function(d) {
        Tooltip
            .style("opacity", 0)
    };

    // Add the points
    svg
        .append("g")
        .selectAll("dot")
        .data(nodes)
        .enter()
        .append("circle")
        .attr("class", "myCircle")
        .attr("cx", function(d) { return x(new Date(d.annee,d.mois-1,d.jour )) } )
        .attr("cy", function(d) { return y(d.value) } )
        .attr("r", 8)
        .attr("stroke", "#69b3a2")
        .attr("stroke-width", 3)
        .attr("fill", "white")
        .on("mouseover", mouseover)
        .on("mousemove", mousemove)
        .on("mouseleave", mouseleave)


}

async function carteRenvois(){


    document.getElementById("contentPopup2").innerHTML="<br><text id=\"p1_title\" class=\"p1_title\" x=\"352.5\" y=\"25.015625\" text-anchor=\"middle\" fill=\"#333333\" style=\"font-size: 18px; font-family: arial;\">Carte des renvois</text><br><br>";
    let data2 = await  d3.json("data/arrets.json");
    let data3 = await  d3.json("data/renvois.json");
    let dataGeo = await d3.json("data/countries.geo.json");
    let nomsPays = await  d3.csv("data/sql-pays.csv");

    dataGeo.features = dataGeo.features.map(country=>{
        country.centerCoord=turf.pointOnFeature(country).geometry.coordinates;
        nomsPays.forEach(p=>{
            if(p.alpha3===country.id) {
                country.nomFR=p.nom_fr_fr;
            }
        });
        country.nbAutorenv=0;
        return country
    })
    data2=data2.map(ar=>{
        if(ar.annee!==0){
            dataGeo.features.forEach(f=>{
                if(ar.etat.toLowerCase()===f.nomFR.toLowerCase()){
                    ar.centerCoord=f.centerCoord;
                }
            })
        }
        ar.autorenv=0;
        return ar;
    });
    console.log(data2,data3,dataGeo)

    //création des objets links et nodes nécéssaires à d3JS avec les valeurs des noeuds pour leur taille etc..
    const links =  data3.map(function(d){
        let sourceVal =-1;
        let targetVal=-2;
        let targetCountry="";
        let sourceCountry="";
        let i=0;
        data2.forEach(function(ar){
            if(ar.nom===d.src){
                sourceVal=i;
                sourceCountry=ar.etat;

            }
            if(ar.nom===d.dst){
                targetVal=i;
                targetCountry=ar.etat;
            }
            i++;
        });
        Object.defineProperty(d, 'displayed', {
            value: 0,
            writable: true
        });
        Object.defineProperty(d, 'source', {
            value: sourceVal,
            writable: true
        });
        Object.defineProperty(d, 'target', {
            value: targetVal,
            writable: true
        });
        Object.defineProperty(d, 'targetCountry', {
            value: targetCountry,
            writable: true
        });
        Object.defineProperty(d, 'sourceCountry', {
            value: sourceCountry,
            writable: true
        });
        Object.defineProperty(d, 'sourceCoord', {
            value: data2[sourceVal].centerCoord,
            writable: true
        });
        Object.defineProperty(d, 'targetCoord', {
            value: data2[targetVal].centerCoord,
            writable: true
        });
        return d
    });
    console.log(links)


// Load world shape AND list of connection


    // Reformat the list of link. Note that columns in csv file are called long1, long2, lat1, lat2
    let link = []
    links.forEach(function(lien){
        let source=0;
        let target=0;
        if(lien.target!==0) {
            if (lien.sourceCountry === lien.targetCountry) {
                console.log("autorenv")
                dataGeo.features.forEach(pa => {
                        if (pa.nomFR.toLowerCase() === lien.sourceCountry.toLowerCase()) {
                            pa.nbAutorenv = pa.nbAutorenv + 1;
                            console.log(pa)
                        }
                    }
                )
            } else {
                target = lien.targetCoord;
                source = lien.sourceCoord;
                //topush = {type: "LineString", coordinates: [source, target]}
                topush = turf.greatCircle(source, target);
                topush.linkData = lien
                topush.displayed=0;
                link.push(topush)
            }
        }
    });
    console.log(dataGeo,link)


    linkSizeScale = d3.scaleLinear()
        .domain([0,10])
        .range([2,8]);

// The svg
    var margin = {top: 10, right: 30, bottom: 30, left: 60},
        width = 460 - margin.left - margin.right,
        height = 400 - margin.top - margin.bottom;


// append the svg object to the body of the page
    var svg = d3.select("#contentPopup2")
        .append("svg")
        .attr("width",700)
        .attr("height", 500)
        .append("g");



    var Tooltip = d3.select("#contentPopup2")
        .append("div")
        .style("opacity", 0)
        .attr("class", "tooltip")
        .style("background-color", "white")
        .style("border", "solid")
        .style("border-width", "2px")
        .style("border-radius", "5px")
        .style("padding", "5px");

    // Three function that change the tooltip when user hover / move / leave a cell


// Map and projection
    var projection = d3.geoMercator()
        .scale(500)
        .translate([width/2.5, height*2.2]);

// A path generator
    var path = d3.geoPath()
        .projection(projection)

    // Draw the map
    let lespays = svg.append("g")
        .selectAll("path")
        .data(dataGeo.features)
        .enter().append("path");
    lespays
        .attr("fill", "#b8b8b8")
        .attr("d", path)
        .style("stroke", "#fff")
        .style("stroke-width", 0)
        .on("mouseover",handleMouseOverPays)
        .on("mouseout",handleMouseOutPays)
        .filter(d=>d.nbAutorenv!==0)
        .attr("fill","orange");


    svg.append("svg:defs").append("svg:marker")
        .attr("id", "triangle2v")
        .attr("refX",5)
        .attr("refY", 3)
        .attr("markerWidth", 10)
        .attr("markerHeight", 20)
        .attr("orient", "auto")
        .append("path")
        .attr("d", "M 0 0 6 3 0 6 1.5 3")
        .style("fill", "#69b3a2");

    svg.append("svg:defs").append("svg:marker")
        .attr("id", "triangle2r")
        .attr("refX",5)
        .attr("refY", 3)
        .attr("markerWidth", 10)
        .attr("markerHeight", 20)
        .attr("orient", "auto")
        .append("path")
        .attr("d", "M 0 0 6 3 0 6 1.5 3")
        .style("fill", "red");
    // Add the path
    let lesliens =svg.selectAll("myPath")
        .data(link)
        .enter()
        .filter(d =>{
            //on affiche qu'un seul lien par groupe de renvois similaires (similaire : même source et même destination
            console.log(d)
            if(d.displayed===0){
                link.forEach(function(renv){
                    if(renv.linkData.source===d.linkData.source&&renv.linkData.target===d.linkData.target){
                        renv.displayed=1;
                    }
                });
                return true}
            else return false;
        })
        .append("path")
        .attr("d", function(d){ return path(d)})
        .style("fill", "none")
        .style("stroke", "#69b3a2")
        .attr("stroke-width", d => linkSizeScale(listeLienSimi(d).length))
        .attr("marker-end", "url(#triangle2v)")
        .on("mouseover",function(d){
            var sel = d3.select(this);
            sel.moveToFront();
            handleMouseOverLien(d);}
            )
        .on("mouseout",handleMouseOutLien);

    let texts  = lespays.append('text')
        .attr('x', function(d){ console.log(path.centroid(d)[0]);return path.centroid(d)[0];})
        .attr('y', function(d){ return path.centroid(d)[1];})
        .text(function(d){return d.nbAutorenv;})
        .style("font-size","14px")
        .style("fill","red");


    d3.selection.prototype.moveToFront = function() {
        return this.each(function(){
            this.parentNode.appendChild(this);
        });
    };

    function handleMouseOverPays(d){

        Tooltip
            .style("opacity", 1);
        svg.selectAll("path").data(dataGeo.features).filter(e => (e.properties.name === d.properties.name)).attr("fill", "red");
        if(d.nbAutorenv!==0){
            Tooltip.html(d.nomFR+": "+d.nbAutorenv+" renvois entre arrêts de ce pays")
        }else {
            Tooltip.html(d.nomFR)
        }
    }
    function handleMouseOutPays(d){
        svg.selectAll("path").data(dataGeo.features).filter( e => (e.properties.name === d.properties.name)).attr("fill", "#b8b8b8").filter(d=>d.nbAutorenv!==0)
            .attr("fill","orange");
        Tooltip
            .style("opacity", 0)
    }

    function handleMouseOverLien(d){

        Tooltip
            .style("opacity", 1);
        lesliens.filter(e =>{return (e.linkData.source===d.linkData.source&&e.linkData.target===d.linkData.target)}).style("stroke","red").attr("marker-end", "url(#triangle2r)");
        Tooltip
            .html( listeLienSimi(d).length+" renvois de "+d.linkData.sourceCountry+" vers "+d.linkData.targetCountry)
    }
    function handleMouseOutLien(d){
        lesliens.filter(e =>{return (e.linkData.source===d.linkData.source&&e.linkData.target===d.linkData.target)}).style("stroke","#69b3a2").attr("marker-end", "url(#triangle2v)");
        Tooltip
            .style("opacity", 0)
    }

    function listeLienSimi(re) {
        let list=[];
        link.forEach(function(renv){
            if(renv.linkData.source===re.linkData.source&&renv.linkData.target===re.linkData.target){
                list.push(renv);
            }
        });
        return list;
    }


}

async function repartitionFonction() {

    document.getElementById("contentPopup2").innerHTML="";
    let renvois =await d3.json("data/renvois.json");
    let types = renvois.map(r => {
        return r.fonction_fond
    });
    let names = [], data = [], prev;
    types.sort();
    for ( let i = 0; i < types.length; i++ ) {
        if ( types[i] !== prev ) {
            names.push(types[i]);
            data.push(1);
        } else {
            data[data.length-1]++;
        }
        prev = types[i];
    }
    data = names.map( (t,i)=>{
        let obj = new Object();
        obj.label=t;
        obj.value=data[i];
        return obj
    });
    let pie = new d3pie("contentPopup2", {
        header: {
            title: {
                text: "Répartition des "+types.length+" renvois selon leur fonction"
            }
        },
        data: {
            content: data
        },
        "labels": {
            "inner": {
                "format": "value",
                "hideWhenLessThanPercentage": 3
            },
            "mainLabel": {
                "fontSize": 16
            },
            "value": {
                "color": "#000",
                "fontSize": 23
            }
        },

//Here further operations/animations can be added like click event, cut out the clicked pie section.
        callbacks: {
            onMouseoverSegment: function(info) {
            },
            onMouseoutSegment: function(info) {
            }
        },
        size: {
            "canvasHeight": 600,
            "canvasWidth": 900,
            "pieInnerRadius": "60%",
            "pieOuterRadius": "75%"
        },

    });
}

async function repartitionObjet_fond() {

    document.getElementById("contentPopup2").innerHTML="";
    let renvois =await d3.json("data/renvois.json");
    let types = renvois.map(r => {
        return r.objet_fond
    });
    let names = [], data = [], prev;
    types.sort();
    for ( let i = 0; i < types.length; i++ ) {
        if ( types[i] !== prev ) {
            names.push(types[i]);
            data.push(1);
        } else {
            data[data.length-1]++;
        }
        prev = types[i];
    }
    data = names.map( (t,i)=>{
        let obj = new Object();
        obj.label=t;
        obj.value=data[i];
        return obj
    });
    let pie = new d3pie("contentPopup2", {
        header: {
            title: {
                text: "Répartition des "+types.length+" renvois selon leur objet (Fond)"
            }
        },
        data: {
            content: data
        },
        "labels": {
            "inner": {
                "format": "value",
                "hideWhenLessThanPercentage": 3
            },
            "mainLabel": {
                "fontSize": 16
            },
            "value": {
                "color": "#000",
                "fontSize": 23
            }
        },

//Here further operations/animations can be added like click event, cut out the clicked pie section.
        callbacks: {
            onMouseoverSegment: function(info) {
            },
            onMouseoutSegment: function(info) {
            }
        },
        size: {
            "canvasHeight": 600,
            "canvasWidth": 900,
            "pieInnerRadius": "60%",
            "pieOuterRadius": "75%"
        },

    });
}

async function repartitionObjet_forme() {

    document.getElementById("contentPopup2").innerHTML="";
    let renvois =await d3.json("data/renvois.json");
    let types = renvois.map(r => {
        return r.objet_forme
    });
    let names = [], data = [], prev;
    types.sort();
    for ( let i = 0; i < types.length; i++ ) {
        if ( types[i] !== prev ) {
            names.push(types[i]);
            data.push(1);
        } else {
            data[data.length-1]++;
        }
        prev = types[i];
    }
    data = names.map( (t,i)=>{
        let obj = new Object();
        obj.label=t;
        obj.value=data[i];
        return obj
    });
    let pie = new d3pie("contentPopup2", {
        header: {
            title: {
                text: "Répartition des "+types.length+" renvois selon leur objet (Forme)"
            }
        },
        data: {
            content: data
        },
        "labels": {
            "inner": {
                "format": "value",
                "hideWhenLessThanPercentage": 3
            },
            "mainLabel": {
                "fontSize": 16
            },
            "value": {
                "color": "#000",
                "fontSize": 23
            }
        },

//Here further operations/animations can be added like click event, cut out the clicked pie section.
        callbacks: {
            onMouseoverSegment: function(info) {
            },
            onMouseoutSegment: function(info) {
            }
        },
        size: {
            "canvasHeight": 600,
            "canvasWidth": 900,
            "pieInnerRadius": "60%",
            "pieOuterRadius": "75%"
        },

    });
}

function closeChart(){
    document.getElementById("contentPopup2").innerHTML="";
}