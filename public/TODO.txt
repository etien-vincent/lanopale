Formats : 
Renvoi{source, dest, type, remarque, extrait}
Arr�t { nom date lien Etat d�fendeur n� requ�te (tous) }


menu options :
-nom de l'appli en haut 
-date 
-afficher infos
-type de renvois
-enlever jurisprudence
-pays
infos sur l'arr�t en bas
(s'ouvre en cliquant sur le nom de l'arr�t)

DONE :
-arr�t ne fesant pas partie du corpus => inCorpus=false
-Bug � corriger : citation balai _> refresh -> clic masse sur paul
-voir tout les arr�ts d'un type => graphique en barre
-rajouter le paragraphe de la citation en mouseOver
-n�paragraphe
-Diff�rents th�mes de couleurs
-bouton dark mode ( document.documentElement.style.setProperty('--my-variable-name', 'pink'); )
-colonne objet secondaire
-selection et affichage � droite: objet fond, fonction fond
-selection sur les arr�ts sources uniquement
-rajouter les commentaires sur l'arret
-pie objet fond, fonction fond , objet forme
-prendre en compte les 2ns et 3e objets
-prendre en compte les arrets hors theme et les afficher avec des pointill�s
-Finir la carte
-export to rtf
-Tout s�lectionner
-corriger renvoie -> renvois
-Adapter � tout types d'�crans (bouton nuit + carte)
-a�rer les fl�ches ( augmenter force de repoussement)


TODO :
-R�flexion sur les couleurs et types de fl�che
-Auto-renvoi : fleche sur soi m�me -> � voir selon le nombre d'occurences
-arr�t qui en cite un, qui en cite un autre -> � voir selon le nombre d'occurences

