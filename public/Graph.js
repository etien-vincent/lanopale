//bouton pour afficher le menu des successeurs
function showRightMenu(){
    var x = document.getElementById("succ");
    if (x.style.display === "none") {
        x.style.display = "block";
        document.getElementById("showSucc").innerText="Cacher";
    } else {
        x.style.display = "none";
        document.getElementById("showSucc").innerText="Afficher";
    }
    showLeftMenu();
}
//bouton pour afficher le menu des prédécesseurs
function showLeftMenu(){
    var x = document.getElementById("preds");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}
//afficher le menu du haut
function showTopMenu(){
    var x = document.getElementById("menu");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}
//bouton pour afficher le menu des infos arrêt
function showBottomMenu(){
    var x = document.getElementById("informations_arret");
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}

//Changement de theme
function lightSwitch(){
    let theme = getComputedStyle(document.body).getPropertyValue('--main-bg-color');

    console.log( theme);
    if(theme==="#424564"){
        document.body.style.setProperty('--main-bg-color', '#eeede7');
        document.body.style.setProperty('--light-menu-color', '#a1cbdd');
        document.body.style.setProperty('--menu-titles-color', '#5997ac');
        document.body.style.setProperty('--light-huee-color', '#f7aec2');
        document.body.style.setProperty('--dark-huee-color', '#af5166');
        document.body.style.setProperty('--contrast-huee-color', '#af5166');
        document.body.style.setProperty('--text-color', '#000');
        document.body.style.setProperty('--text-color-buttons', '#000');
        document.body.style.setProperty('--text-color-outer-buttons', '#000');
        document.body.style.setProperty('--title-color', '#fff');
        document.getElementById("lightSwitch").innerText="Jour";
    }else{
        document.body.style.setProperty('--main-bg-color', '#424564');
        document.body.style.setProperty('--light-menu-color', '#e6f0fc');
        document.body.style.setProperty('--menu-titles-color', '#86a4ca');
        document.body.style.setProperty('--light-huee-color', '#5dddd0');
        document.body.style.setProperty('--dark-huee-color', '#874a97');
        document.body.style.setProperty('--contrast-huee-color', '#874a97');
        document.body.style.setProperty('--text-color', '#000');
        document.body.style.setProperty('--text-color-buttons', '#000');
        document.body.style.setProperty('--text-color-outer-buttons', '#fff');
        document.body.style.setProperty('--title-color', '#fff');
        document.getElementById("lightSwitch").innerText="Nuit";

    }
    refreshData();

}

//filtre permettant d'éliminer les doublons d'un tableau
function onlyUnique(value, index, self) {
    return self.indexOf(value) === index;
}

let from=1970;
let to=2018;
//récupération des valeurs du range-slider pour les dates
function getVals(data){
    from=data.from;
    to=data.to;
}

window.onload = function(){
    // Initialize Sliders
    let sliders = document.getElementsByTagName("input");
    if(screen.availHeight<1000||screen.availWidth<1800){
        document.body.style.zoom = "90%"
    }
    if(screen.availHeight>1200||screen.availWidth>2000){
        document.body.style.zoom = "60%"
    }

    for( let y = 0; y < sliders.length; y++ ){
        if( sliders[y].type ==="range" ){
            sliders[y].oninput = getVals;
            // Manually trigger event first time to display values
            sliders[y].oninput();
        }
    }

}

//fonction de récupération des données, prends en compte les paramètres du menu ( année, type de renvoi ...)
async function getData() {
    let data1 = await d3.json("data/arrets.json");
    let data3 = await d3.json("data/renvois.json");

    //dédoublement des renvois pour ceux qui ont des renvois secondaires et tertiaires
    if (document.getElementById("renvoisSecondaires").checked) {
    let data4 = [];
    let newrenv;
    data3.forEach(renv => {
        data4.push(renv)
        if (renv.obj2 !== ""&&renv.obj2!==undefined) {
            newrenv = jQuery.extend({}, renv);
            newrenv.dst = renv.obj2;
            data4.push(newrenv)
        }
        if (renv.obj3 !== ""&&renv.obj3!==undefined) {
            newrenv = jQuery.extend({}, renv);
            newrenv.dst = renv.obj3;
            data4.push(newrenv)
        }
        if (renv.obj4 !== ""&&renv.obj4!==undefined) {
            newrenv = jQuery.extend({}, renv);
            newrenv.dst = renv.obj4;
            data4.push(newrenv)
        }
        if (renv.obj5 !== ""&&renv.obj5!==undefined) {
            newrenv = jQuery.extend({}, renv);
            newrenv.dst = renv.obj5;
            data4.push(newrenv)
        }
        if (renv.obj6 !== ""&&renv.obj6!==undefined) {
            newrenv = jQuery.extend({}, renv);
            newrenv.dst = renv.obj6;
            data4.push(newrenv)
        }
        if (renv.obj7 !== ""&&renv.obj7!==undefined) {
            newrenv = jQuery.extend({}, renv);
            newrenv.dst = renv.obj7;
            data4.push(newrenv)
        }
    })
    data3 = data4
}
    //filtre sur les années
    let data2 = data1.filter(a=>{
        return (a.annee>=from)&&(a.annee<=to)||((a.annee===0)&&document.getElementById("juris").checked)
    });

    //filtre sur les Etats défendeurs
    if(checkboxesMenus) {
        data2 = data2.filter(a => {
            boxes = document.getElementById("checkboxesPays").childNodes;

            for (let i = 0; i < boxes.length; i++) {
                if(boxes[i].getAttribute("for")===a.etat){
                    if(boxes[i].childNodes[1].checked){
                        return true
                    }else return false;
                }
            }
            return true;
        });

        //filtre sur les types de renvois
        data3 = data3.filter(r => {
            boxes = document.getElementById("checkboxesTypes").childNodes;
            for (let i = 0; i < boxes.length; i++) {
                if(boxes[i].getAttribute("for")===r.type){
                    if(boxes[i].childNodes[1].checked){
                        return true
                    }else return false;
                }
            }
            return true;
        });

        //filtre sur les Objets(Fond)
        data3 = data3.filter(r => {
            boxes = document.getElementById("checkboxesObjet_fond").childNodes;
            for (let i = 0; i < boxes.length; i++) {
                if(boxes[i].getAttribute("for")===r.objet_fond){
                    if(boxes[i].childNodes[1].checked){
                        return true
                    }else return false;
                }
            }
            return true;
        });

        //filtre sur les Objets(Forme)
        data3 = data3.filter(r => {
            boxes = document.getElementById("checkboxesObjet_forme").childNodes;
            for (let i = 0; i < boxes.length; i++) {
                if(boxes[i].getAttribute("for")===r.objet_forme){
                    if(boxes[i].childNodes[1].checked){
                        return true
                    }else return false;
                }
            }
            return true;
        });

        //filtre sur les Fonctions
        data3 = data3.filter(r => {
            boxes = document.getElementById("checkboxesFonction").childNodes;
            for (let i = 0; i < boxes.length; i++) {
                if(boxes[i].getAttribute("for")===r.fonction_fond){
                    if(boxes[i].childNodes[1].checked){
                        return true
                    }else return false;
                }
            }
            return true;
        });
    }

    //enlève les renvois qui sont fait entre des noeuds non-présents dans la base de donnée (à cause du tri)
    let names = data2.map(a => a.nom);
    data3 = data3.filter(r=>{
        return (names.includes(r.src))
    });
    //rajout des noeuds qui sont cités par des noeuds encore présents sauf jurisprudence
    let dests = data3.map(r=>r.dst);
    data1=data1.filter(a=> {
        return (dests.includes(a.nom)&&a.nom!=="Jurisprudence")
    });

    //on enlève les renvois à jurisprudence si la case n'est pas cochée pour éviter les bugs de renvois vers un noeud non-existant
    if(!document.getElementById("juris").checked){
        data3 = data3.filter(r=>{
            return (r.dst!=="Jurisprudence")
        });
    }

    data2=data2.concat(data1).filter(onlyUnique);
    console.log(data2)
    //création des objets links et nodes nécéssaires à d3JS avec les valeurs des noeuds pour leur taille etc..
    const links =  data3.map(function(d){
        let sourceVal =-1;
        let targetVal=-2;
        let i=0;
        data2.forEach(function(ar){
            if(ar.nom===d.src){
                sourceVal=i;
            }
            if(ar.nom===d.dst){
                targetVal=i;
            }
            i++;
        });
        return Object.create(d, { displayed: {value:0,writable: true} ,source: {value: sourceVal,writable: true} ,target: {value:targetVal,writable: true} })
    });
    let nodes =  data2.map((d, index) => Object.create(d, { id: { value: index } }));
    nodes = nodes.map(d=> {
        let nb=0;
        links.forEach(function(renv){
            if(renv.target===d.id){
                nb++
            }
        });
        d.value=nb;
        return d
    } );

    return [data2,data3,nodes,links]
}





async function main(data2,data3,nodes,links) {
    //initialisation de la forceSimulation, les paramètres de forces,éloignements,... entre les noeuds sont ici
    function forceSimulation(nodes, links) {
        return d3.forceSimulation(nodes)
            .force("link", d3.forceLink(links).id(d => d.id).strength(d=>listeLienSimi(d).length/10000))
            .force("charge", d3.forceManyBody().strength(-500).distanceMin(0).distanceMax(100))
            .force("center", d3.forceCenter().x(width / 100).y(height / 100))
            //.force("collide",d3.forceCollide([d=>valueScale(d.value+3)]))
            //.force("radial",d3.forceRadial().strength(1).radius(200));
    }
    /* TODO: EXEMPLE DE PARAMETRES intéressants, à tester dans le futur, lier la force du link au nb de liensSimi
    networkChart.force = d3.layout.force().size([w, h])
	.nodes(networkChart.nodes).links(networkChart.links)
	.gravity(1).linkDistance(100).charge(-3000)
	.linkStrength(function(x) {
		return x.weight * 10
	});
    */

    //SetUp affichage liste Pays et liste des types (le paramètre checkboxesMenus dans le if est initialisé au début du programme à false et est ensuite à true pour toujours
    if (!checkboxesMenus) {

        //Set Up checkboxes Etat défendeur
        let etats = data2.map(a => a.etat);
        etats = etats.filter(e => {
            return (e !== undefined)&&(e!=="")
        });
        etats=etats.filter(onlyUnique);
        var checkboxesEtats = document.getElementById("checkboxesPays");
        while (checkboxesEtats.firstChild) {
            checkboxesEtats.removeChild(checkboxesEtats.firstChild);
        }
        checkboxesEtats.innerHTML+="<label for=\"togglePays\"><input type=\"checkbox\" id=\"togglePays\" onclick=\"togglePays(this)\" checked/>TOUT SELECTIONNER</label>"
        etats.forEach(etat => {
            checkboxesEtats.innerHTML += "<label for=\"" + etat + "\"> <input type=\"checkbox\" id=\"" + etat + "\" checked/>" + etat + "</label>"
        });

        //Set Up checkboxes Types
        let types = data3.map(r => r.type);
        types = types.filter(t => {
            return t !== undefined
        });
        types=types.filter(onlyUnique);
        var checkboxesTypes = document.getElementById("checkboxesTypes");
        while (checkboxesTypes.firstChild) {
            checkboxesTypes.removeChild(checkboxesTypes.firstChild);
        }
        checkboxesTypes.innerHTML+="<label for=\"toggleTypes\"><input type=\"checkbox\" id=\"toggleTypes\" onclick=\"toggleTypes(this)\" checked/>TOUT SELECTIONNER</label>"
        types.forEach(type => {
            checkboxesTypes.innerHTML += "<label for=\"" + type + "\"> <input type=\"checkbox\" id=\"" + type + "\" checked/>" + type + "</label>"
        });

        //Set Up checkboxes Objet(Fond)
        let objets_fond = data3.map(r => r.objet_fond);
        objets_fond = objets_fond.filter(t => {
            return t !== undefined
        });
        objets_fond=objets_fond.filter(onlyUnique);
        var checkboxesObjet_fond = document.getElementById("checkboxesObjet_fond");
        while (checkboxesObjet_fond.firstChild) {
            checkboxesObjet_fond.removeChild(checkboxesObjet_fond.firstChild);
        }
        checkboxesObjet_fond.innerHTML+="<label for=\"toggleObjet_fond\"><input type=\"checkbox\" id=\"toggleObjet_fond\" onclick=\"toggleObjet_fond(this)\" checked/>TOUT SELECTIONNER</label>"
        objets_fond.forEach(type => {
            checkboxesObjet_fond.innerHTML += "<label for=\"" + type + "\"> <input type=\"checkbox\" id=\"" + type + "\" checked/>" + type + "</label>"
        });

        //Set Up checkboxes Objet(Forme)
        let objets_forme = data3.map(r => r.objet_forme);
        objets_forme = objets_forme.filter(t => {
            return t !== undefined
        });
        objets_forme=objets_forme.filter(onlyUnique);
        var checkboxesObjet_forme = document.getElementById("checkboxesObjet_forme");
        while (checkboxesObjet_forme.firstChild) {
            checkboxesObjet_forme.removeChild(checkboxesObjet_forme.firstChild);
        }
        checkboxesObjet_forme.innerHTML+="<label for=\"toggleObjet_forme\"><input type=\"checkbox\" id=\"toggleObjet_forme\" onclick=\"toggleObjet_forme(this)\" checked/>TOUT SELECTIONNER</label>"
        objets_forme.forEach(type => {
            checkboxesObjet_forme.innerHTML += "<label for=\"" + type + "\"> <input type=\"checkbox\" id=\"" + type + "\" checked/>" + type + "</label>"
        });
        //Set Up checkboxes Fonction
        let fonctions = data3.map(r => r.fonction_fond);
        fonctions = fonctions.filter(t => {
            return t !== undefined
        });
        fonctions=fonctions.filter(onlyUnique);
        var checkboxesFonction = document.getElementById("checkboxesFonction");
        while (checkboxesFonction.firstChild) {
            checkboxesFonction.removeChild(checkboxesFonction.firstChild);
        }
        checkboxesFonction.innerHTML+="<label for=\"toggleFonction\"><input type=\"checkbox\" id=\"toggleFonction\" onclick=\"toggleFonction(this)\" checked/>TOUT SELECTIONNER</label>"
        fonctions.forEach(type => {
            checkboxesFonction.innerHTML += "<label for=\"" + type + "\"> <input type=\"checkbox\" id=\"" + type + "\" checked/>" + type + "</label>"
        });

        checkboxesMenus=true;
    }


    const height = 480;
    const width = 858;

    //scale pour la taille des noeuds
    valueScale = d3.scaleLinear()
        .domain([0,25])
        .range([20,50]);

    //scale pour la taille des liens
    linkSizeScale = d3.scaleLinear()
        .domain([0,10])
        .range([0,6]);

    //scale pour la couleur des noeuds ( le black est pour la jurisprudence (ayant une année =0)
    colorScale = d3.scaleLinear()
        .domain([0,1976,1997,2018])
        .range(["black","#ff6961","#77dd77","#063971"]);




    const simulation = await forceSimulation(nodes, links).on("tick", ticked);


    let drag = simulation => {

        function dragStarted(d) {
            handleMouseClick(d);
            if (!d3.event.active) simulation.alphaTarget(0.1).restart();
            d.fx = d.x;
            d.fy = d.y;
        }

        function dragged(d) {
            d.fx = d3.event.x;
            d.fy = d3.event.y;
        }

        function dragEnded(d) {
            if (!d3.event.active) simulation.alphaTarget(0);
            d.fx = null;
            d.fy = null;
        }

        return d3.drag()
            .on("start", dragStarted)
            .on("drag", dragged)
            .on("end", dragEnded);
    };

    function handleMouseClick(d){

        //changement de couleur du noeud cliqué en orange et de l'ancien noeud cliqué en blanc
        svg.selectAll("circle").data(nodes).filter( e => (e.id===d.id)).attr("stroke", "#D75813").attr("clicked","true");
        svg.selectAll("circle").data(nodes).filter( e => (e.id!==d.id)).attr("stroke", getComputedStyle(document.body).getPropertyValue('--text-color-outer-buttons')).attr("clicked","false");

        //Si c'est la jurisprudence, le titre du menu en bas est juste "jurisprudence" , sinon c'est "arrêt nomarrêt"
        if(d.annee===0){document.getElementById("nom_arret").innerHTML = d.nom;}else{document.getElementById("nom_arret").innerHTML = "Arrêt "+d.nom;}

        //on vide la zone info arrêt , la zone infos prédécesseurs et la zone infos successeurs
        var infoZone = document.getElementById("zone_infos_arret");
        while (infoZone.firstChild) {
            infoZone.removeChild(infoZone.firstChild);
        }
        var infoZonePreds = document.getElementById("preds");
        while (infoZonePreds.firstChild) {
            infoZonePreds.removeChild(infoZonePreds.firstChild);
        }
        var infoZoneSuccs = document.getElementById("succ");
        while (infoZoneSuccs.firstChild) {
            infoZoneSuccs.removeChild(infoZoneSuccs.firstChild);
        }

        //remplissage de la zone infos arrêt
        if(d.annee===0){infoZone.innerHTML += "<div class=\"info_arret\" >Ce document corresponds au renvois<br> \"In globo\" fait à la Jurisprudence de la cour </div><br>"+"<div class=\"info_arret\" >" +"accès au corpus : <a target=\"_blank\" href=\"" + d.lien + "\">🔗</a>" +
            "</div>";}else {
            infoZone.innerHTML += "<div class=\"info_arret\" >" +
                "n°requête : " + d.requete +
                "</div><br>" +
                "<div class=\"info_arret\" >" +
                "Etat défendeur : " + d.etat +
                "</div><br>" +
                "<div class=\"info_arret\" >" +
                "date : " + d.jour + "/" + d.mois + "/" + d.annee +
                "</div><br>" +
                "<div class=\"info_arret\" >" +
                "accès au document : <a target=\"_blank\" href=\"" + d.lien + "\">🔗</a>" +
                "</div>";
        }
        if(d.Commentaire!==""&&d.Commentaire!==undefined){
            infoZone.innerHTML +="<br>" +
                "<div class=\"info_arret\" >" +
                "commentaire : " + d.Commentaire +
                "</div>"
        }
        if((d.inTheme==="FALSE")){
            infoZone.innerHTML +="<br>" +
                "<div class=\"info_arret\" >" +
                "Cet arrêt ne fait pas partie du corpus car il ne traitait pas de la définition de la matière pénale"+
                "</div>"
        }

        //titre zone infos prédecesseurs
        if(d.value===0){document.getElementById("preds").innerHTML = "<div class=\"nbRenvois\"><span class=\"information_title\"  >Pas de renvoi à ce document</span></div><div class=\"sliderContainer\" id=\"predsContainer\">\n" +
            "    </div>";}
        else if (d.value===1){document.getElementById("preds").innerHTML = "<div class=\"nbRenvois\"> <span class=\"information_title\"  >1 renvoi à ce document :</span></div><div class=\"sliderContainer\" id=\"predsContainer\">\n" +
            "    </div>";}
        else {document.getElementById("preds").innerHTML = " <div class=\"nbRenvois\"><span class=\"information_title\" >"+d.value+" renvois à ce document :</span></div><div class=\"sliderContainer\" id=\"predsContainer\">\n" +
            "    </div>";}


        //remplissage zone infos predecesseurs
        let listePredsPrinted=[];
        let listeDePreds=listePreds(d);
        listeDePreds.forEach( predec=> {
            if(!listePredsPrinted.includes(data3[predec.index])){
            let simi = listeLienSimi(predec);
            document.getElementById("predsContainer").innerHTML += "<div class=\"preds_succs\" id=\"" + predec.index + predec.source.nom+"preds\"><span class='arrNamesInList' >De " + predec.source.nom + "</span><br></div>";
            simi.forEach(renvoi => {
                document.getElementById(predec.index + predec.source.nom + "preds").innerHTML += "<div class=\"singleRenvoi\"><span class=\"categRenvoi\">§"+renvoi.npara+" : </span>\"" + renvoi.extrait + "\"<br> " + "<span class=\"categRenvoi\">Notes :</span> " + renvoi.remarque + "<br>" + "<span class=\"categRenvoi\">Type :</span> " + renvoi.type + "<br><span class=\"categRenvoi\">Objet (Fond) : </span>"+renvoi.objet_fond+"<br><span class=\"categRenvoi\">Objet (Forme) : </span>"+renvoi.objet_forme+"<br><span class=\"categRenvoi\">Fonction : </span>"+renvoi.fonction_fond+"<span class=\"paraPred\">"+renvoi.context+"</span></div><br><br>";
                listePredsPrinted.push(renvoi);

            });
            //document.getElementById("predsContainer").innerHTML += "</div>";
            }
            });






        //titre zone infos successeurs
        let liste = listeSuccs(d);
        if(d.inCorpus==="FALSE"){document.getElementById("succ").innerHTML = "<div class=\"nbRenvois\"><span class=\"information_title\"  >Ce document ne fait pas partie du corpus étudié</span></div>";}
        else if(liste.length===0){document.getElementById("succ").innerHTML = "<div class=\"nbRenvois\"><span class=\"information_title\"  >Ce document ne renvoie à aucun autre</span></div>";}
        else {document.getElementById("succ").innerHTML = " <div class=\"nbRenvois\"><span class=\"information_title\">Ce document renvoie "+liste.length+" fois</span></div><div class=\"sliderContainer\" id=\"succsContainer\">\n" +
            "    </div>";}

        //remplissage zone infos sucesseurs
        let listeSuccPrinted=[];
        liste.forEach( succes=> {
            if(!listeSuccPrinted.includes(data3[succes.index])) {
                let simi = listeLienSimi(succes);
                document.getElementById("succsContainer").innerHTML += "<div class=\"preds_succs\" id=\"" + succes.index + succes.target.nom + "succ\"><span class='arrNamesInList' >vers " + succes.target.nom + "</span><br></div>";
                simi.forEach(renvoi => {
                    document.getElementById(succes.index + succes.target.nom + "succ").innerHTML += "<div class=\"singleRenvoi\"><span class=\"categRenvoi\">§"+renvoi.npara+" : </span>\"" + renvoi.extrait + "\"<br> " + "<span class=\"categRenvoi\">Notes : </span>" + renvoi.remarque + "<br>" + "<span class=\"categRenvoi\">Type :</span>" + renvoi.type +"<br><span class=\"categRenvoi\">Objet (Fond) : </span>"+renvoi.objet_fond+"<br><span class=\"categRenvoi\">Objet (Forme) : </span>"+renvoi.objet_forme+"<br><span class=\"categRenvoi\">Fonction : </span>"+renvoi.fonction_fond+ "<span class=\"paraSucc\">"+renvoi.context+"</span></div><br><br>";
                    listeSuccPrinted.push(renvoi);
                });
                //document.getElementById("succsContainer").innerHTML += "</div>";
            }
        });




    }
    //changements de couleurs lors du mouseOver
    function handleMouseOver(d){
        svg.selectAll("circle").data(nodes).filter(e => (e.id === d.id)).attr("stroke", "red");
    }
    function handleMouseOut(d){
            if(svg.selectAll("circle").data(nodes).filter( e => (e.id===d.id)).attr("clicked")==="true"){
                svg.selectAll("circle").data(nodes).filter( e => (e.id===d.id)).attr("stroke", "#D75813");
            }else{
                svg.selectAll("circle").data(nodes).filter( e => (e.id===d.id)).attr("stroke", getComputedStyle(document.body).getPropertyValue('--text-color-outer-buttons'));
            }
    }



    //svg : zone de dessin, les deux premiers paramètres sont la position du centre de la zone, les deux seconds la largeur et hauteur de cette zone
    const svg =  d3.select("#graphHolder").append("svg")
        .attr("width", "100%").attr("height", "100%")
        .attr("viewBox", [-width/2,-height/2, width, height])
        .call(d3.zoom().on("zoom", function () {
            svg.attr("transform", d3.event.transform)
        }));


    //création du triangle de bout de flèche
    svg.append("svg:defs").append("svg:marker")
        .attr("id", "triangle")
        .attr("refX",5)
        .attr("refY", 3)
        .attr("markerWidth", 10)
        .attr("markerHeight", 20)
        .attr("orient", "auto")
        .append("path")
        .attr("d", "M 0 0 6 3 0 6 1.5 3")
        .style("fill", getComputedStyle(document.body)
            .getPropertyValue('--text-color-outer-buttons'));

    //création des liens
    const link = svg.append("g")
        .attr("stroke", getComputedStyle(document.body)
            .getPropertyValue('--text-color-outer-buttons'))
        .selectAll("line")
        .data(links)
        .enter()
        .filter(d =>{
            //on affiche qu'un seul lien par groupe de renvois similaires (similaire : même source et même destination
            if(d.displayed===0){
                links.forEach(function(renv){
                    if(renv.target===d.target&&renv.source===d.source){
                        renv.displayed=1;
                    }
                });
                return true}
            else return false;
        })
        .append("line")
        //.style("stroke-dasharray", ("1, 7")) //si on veut appliquer un style de ligne ( éventuellement utiliser une fonction pour que ce soit un paramètres du type de renvoi ou autre )
        .attr("stroke-width", d => linkSizeScale(listeLienSimi(d).length))
        .attr("marker-end", "url(#triangle)");

    //création des noeuds
    const node = svg.append("g")
        .attr("stroke", getComputedStyle(document.body).getPropertyValue('--text-color-outer-buttons'))
        .attr("stroke-width", 1.5)
        .selectAll("circle")
        .data(nodes)
        .enter().append("circle")
        .attr("r", d => valueScale(d.value))
        .attr("fill", d=> colorScale(d.annee))
        .style("stroke-dasharray", d=> {if((d.inTheme==="FALSE")){return("3, 7")}})
        .call(drag(simulation))
        .on("click", d => handleMouseClick(d))
        .on("mouseover",handleMouseOver)
        .on("mouseout",handleMouseOut);

    //ajout des textes
    let texts  = svg.selectAll(".texts")
        .data(nodes)
        .enter()
        //.filter( d => ((valueScale(d.value)) >= 25))
        .append("text")
        .attr("font-family", "sans-serif")
        .attr("text-anchor", "middle")
        .attr("dy", "0.35em")
        .attr("fill","white")
        .attr("font-size", function(d) {
            //on adapte la taille de police à la taille du texte et à la taille du noeud
            let xPadding, diameter, labelAvailableWidth, labelWidth;
            xPadding = 10;
            diameter = 2 * valueScale(d.value);
            labelAvailableWidth = diameter - xPadding;
            labelWidth = d.nom.length*8;
            // There is enough space for the label so leave it as is.
            if (labelWidth < labelAvailableWidth) {
                return null;
            }
            return (labelAvailableWidth / labelWidth) + 'em';
        })
        .text( d => d.nom )
        .style('pointer-events', 'auto')
        .call(drag(simulation))
        //le drag et clic doit être possible sur les textes aussi, sinon on est obligé de cliquer sur les bords du noeud
        .on("click", d => handleMouseClick(d))
        .on("mouseover",handleMouseOver)
        .on("mouseout",handleMouseOut);


    //à chaque tick on update la position des links, des textes et des noeuds
    function ticked() {
        //obligé de faire de la trigo pour obtenir la position en bord de cercle des coordonées, sinon la flèche serait au milieu du noeud et pas sur le coté, on verrai plus rien

        node
            .attr("cx", d => d.x=Math.max(-width/2,Math.min(width/2,d.x)))
            .attr("cy", d => d.y=Math.max(-height/2+30,Math.min(height/2-30,d.y)));

        link
            .attr("x1", d => d.source.x+valueScale(d.source.value)*Math.cos(angle(d)))
            .attr("y1", d => d.source.y+valueScale(d.source.value)*Math.sin(angle(d)))
            .attr("x2", d => d.target.x-valueScale(d.target.value)*Math.cos(angle(d)))
            .attr("y2", d => d.target.y-valueScale(d.target.value)*Math.sin(angle(d)));

        texts
            .attr("x", d => d.x)
            .attr("y", d => d.y);

    }

    //calcul de l'angle entre l'axe horizontal et le lien, nécéssaire pour effectuer la trigo d'au dessus
    function angle(d){
        let angle=(Math.PI/2)*Math.sign(d.target.y-d.source.y);
        if(d.target.x<d.source.x){
            angle = Math.PI+Math.atan((d.target.y-d.source.y)/(d.target.x-d.source.x));
        }else if(d.target.x>d.source.x){
            angle = Math.atan((d.target.y-d.source.y)/(d.target.x-d.source.x));
        }
        return angle;
    }

    //liste des renvois vers ce document
    function listePreds(ar){
        let list=[];
        links.forEach(function(renv){
            if(renv.target.id===ar.id){
                list.push(renv);
            }
        });
        return list;
    }
    //liste des renvois depuis ce document vers un autre
    function listeSuccs(ar){
        let list=[];
        links.forEach(function(renv){
            if(renv.source.id===ar.id){
                list.push(renv);
            }
        });
        return list;
    }

    //liste des renvois ayant le même destinataire et la même source
    function listeLienSimi(re) {
        re = data3[re.index];
        let list=[];
        data3.forEach(function(renv){
            if(renv.dst===re.dst&&renv.src===re.src){
                list.push(renv);
            }
        });
        return list;
    }


}




async function refreshData(){
    //on vide le graph
    d3.select("#graphHolder").selectAll("*").remove();
    //on récupère les données avec les nouveaux paramètres
    dataList = await getData();

    //on réinitialise toute les zones de textes
    document.getElementById("nom_arret").innerHTML = "Infos Arrêt ";
    document.getElementById("zone_infos_arret").innerHTML ="<div class=\"info_arret\" >\n" +
        "      cliquez sur un arrêt pour avoir des informations sur celui-ci\n" +
        "    </div>";

    var infoZonePreds = document.getElementById("preds");
    while (infoZonePreds.firstChild) {
        infoZonePreds.removeChild(infoZonePreds.firstChild);
    }
    document.getElementById("preds").innerHTML = "<div class=\"nbRenvois\"><span class=\"information_title\"  >Informations :</span></div><div class=\"sliderContainer\" id=\"predsContainer\">\n" +
        "    </div>";
    var infoZonePreds = document.getElementById("succ");
    while (infoZonePreds.firstChild) {
        infoZonePreds.removeChild(infoZonePreds.firstChild);
    }
    document.getElementById("succ").innerHTML = "<div class=\"nbRenvois\"><span class=\"information_title\"  >Informations :</span></div><div class=\"sliderContainer\" id=\"predsContainer\">\n" +
        "    </div>";

    //on relance le graph
    main(dataList[0],dataList[1],dataList[2],dataList[3]);

}
//fonctions nécéssaires en JS à la présence de checkboxes dans une liste déroulante
var expandedPays = false;
function showCheckboxesPays() {
    var checkboxes = document.getElementById("checkboxesPays");
    if (!expandedPays) {
        checkboxes.style.display = "block";
        expandedPays = true;
    } else {
        checkboxes.style.display = "none";
        expandedPays = false;
    }
}
function togglePays(source) {
    checkboxes = document.getElementById('checkboxesPays').childNodes;
    console.log(checkboxes)
    for(var i=0, n=checkboxes.length;i<n;i++) {
        console.log(checkboxes[i].childNodes[1])
        checkboxes[i].childNodes[1].checked = source.checked;
    }
}

var expandedTypes = false;
function showCheckboxesTypes() {
    var checkboxes = document.getElementById("checkboxesTypes");
    if (!expandedTypes) {
        checkboxes.style.display = "block";
        expandedTypes = true;
    } else {
        checkboxes.style.display = "none";
        expandedTypes = false;
    }
}
function toggleTypes(source) {
    checkboxes = document.getElementById('checkboxesTypes').childNodes;
    console.log(checkboxes)
    for(var i=0, n=checkboxes.length;i<n;i++) {
        console.log(checkboxes[i].childNodes[1])
        checkboxes[i].childNodes[1].checked = source.checked;
    }
}

var expandedFonction = false;
function showCheckboxesFonction() {
    var checkboxes = document.getElementById("checkboxesFonction");
    if (!expandedFonction) {
        checkboxes.style.display = "block";
        expandedFonction = true;
    } else {
        checkboxes.style.display = "none";
        expandedFonction = false;
    }
}
function toggleFonction(source) {
    checkboxes = document.getElementById('checkboxesFonction').childNodes;
    console.log(checkboxes)
    for(var i=0, n=checkboxes.length;i<n;i++) {
        console.log(checkboxes[i].childNodes[1])
        checkboxes[i].childNodes[1].checked = source.checked;
    }
}


var expandedObjet_forme = false;
function showCheckboxesObjet_forme() {
    var checkboxes = document.getElementById("checkboxesObjet_forme");
    if (!expandedObjet_forme) {
        checkboxes.style.display = "block";
        expandedObjet_forme = true;
    } else {
        checkboxes.style.display = "none";
        expandedObjet_forme = false;
    }
}
function toggleObjet_forme(source) {
    checkboxes = document.getElementById('checkboxesObjet_forme').childNodes;
    console.log(checkboxes)
    for(var i=0, n=checkboxes.length;i<n;i++) {
        console.log(checkboxes[i].childNodes[1])
        checkboxes[i].childNodes[1].checked = source.checked;
    }
}

var expandedObjet_fond = false;
function showCheckboxesObjet_fond() {
    var checkboxes = document.getElementById("checkboxesObjet_fond");
    if (!expandedObjet_fond) {
        checkboxes.style.display = "block";
        expandedObjet_fond = true;
    } else {
        checkboxes.style.display = "none";
        expandedObjet_fond = false;
    }
}
function toggleObjet_fond(source) {
    checkboxes = document.getElementById('checkboxesObjet_fond').childNodes;
    console.log(checkboxes)
    for(var i=0, n=checkboxes.length;i<n;i++) {
        console.log(checkboxes[i].childNodes[1])
        checkboxes[i].childNodes[1].checked = source.checked;
    }
}

let checkboxesMenus=false;
//lancement du programme
refreshData();
